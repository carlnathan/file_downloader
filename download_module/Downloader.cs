﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.ComponentModel;

namespace download_module
{
    //Responsible for accepting the list of URLs, iterating over them and triggering a download for each of them
    public class Downloader
    {
        public ProgressChanged ProgressChangedCallBack;
        public DownloadCompleted DownloadCompleteCallBack;
        public GetUserCredentials GetUserCredentials;
        public Log Log;

        public void Download(string[] Sources, string OutputDir)
        {
            Log("Starting download...");
            //Setup the temp folder:
            //Create temp folder if it doesn't exist yet
            if (!System.IO.Directory.Exists(Globals.tempDir))
            {
                System.IO.Directory.CreateDirectory(Globals.tempDir);
            }


            //prefix the fileName with a unique random id
            var rnd = new Random();
            foreach (string source in Sources)
            {
                Download dl = new Download(rnd.Next(1,1000),source, OutputDir);
                dl.ProgressChangedCallBack = ProgressChangedCallBack;
                dl.DownloadCompletedCallback = DownloadCompleteCallBack;
                dl.Log = Log;
                dl.Start();
            }

            //todo: create this as a thread / wait for all downloads to finish before Logging below
            Log("Downloads have completed");

            //todo:
            //make sure all the download threads have finished, disposed, and then delete the temp folder

        }
    }

    public static class Globals
    {
        public static string tempDir
        {
            get { return System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\temp\\"; }
        }
    }

    public struct CoOrds
    {
        public int x, y;

        public CoOrds(int p1, int p2)
        {
            x = p1;
            y = p2;
        }
    }

}
