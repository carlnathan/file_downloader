﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace download_module
{
    public delegate void ProgressChanged(string fileName, DownloadProgressChangedEventArgs e);
    public delegate void DownloadCompleted(string url, string filePath, string fileName, AsyncCompletedEventArgs e);
    public delegate string[] GetUserCredentials(string url);
    public delegate void Log(string message);

    //does the dirty work of downloading the file, and moving the file to the downloads folder once the download has succeeded
    class Download : IDisposable
    {
        WebClient wc;
        Uri uri;

        string fileName;
        string saveToDir;
        string saveToFilePath;
        string tempFilePath;

        
        public ProgressChanged ProgressChangedCallBack;
        public DownloadCompleted DownloadCompletedCallback;
        public GetUserCredentials Credentials;
        public Log Log;
        

        public Download(int id, string uri, string saveToDir)
        {
            this.uri = new Uri(uri);
            this.saveToDir = saveToDir;

            wc = new WebClient();
            wc.DownloadProgressChanged += this.progressChanged;
            wc.DownloadFileCompleted += downloadCompleted;
            wc.Headers.Add("User-Agent: Other");

            fileName =generateFileName(id);
            saveToFilePath = saveToDir + "\\" + fileName;
            tempFilePath = Globals.tempDir + fileName;
        }
        
        private string generateFileName(int id)
        {
            string[] fileNameandFileType = System.IO.Path.GetFileName(this.uri.LocalPath).Split('.');

            //filename_TimeStamp_uniqueID_.fileType
            List<string> fileName = new List<string>();
            for(int x = 0; x<fileNameandFileType.Length-1; x++)
            {
                fileName.Add(fileNameandFileType[x]);
            }
            return string.Join(".",fileName) + "_" + DateTime.Now.ToString("yyMMddHHmmss") +"_" + id + "." + fileNameandFileType[fileNameandFileType.Length-1];
            
        }

        public void Dispose()
        {
            wc.Dispose();
        }

        public void Start()
        {
            try
            {
                //download file to temp folder
                //Console.WriteLine("{0} Starting download", fileName);
                wc.DownloadFileAsync(uri, tempFilePath);
            }
            catch (WebException ex)
            {
                var response = (HttpWebResponse)ex.Response;

                //handle unauthorized exception
                if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    string[] credentials;
                    if (Credentials != null)
                    {
                        credentials = this.Credentials(uri.AbsoluteUri);

                        wc.Credentials = new System.Net.NetworkCredential("user", "password");
                        this.Start();
                    }
                }
                else //all other types of errors
                {
                    Log("An error has occured while downloading "+fileName+". "+ex.Message);
                }
                
            }
        }

        private void downloadCompleted(object sender, AsyncCompletedEventArgs e)
        {
            //Does the target Dir exist?
            if (!System.IO.Directory.Exists(saveToDir))
            {
                System.IO.Directory.CreateDirectory(saveToDir);
            }
            //move downloaded file from temp to target Dir
            System.IO.File.Move(tempFilePath, saveToFilePath);
            //wait for the thread to finish before disposing
            //this.Dispose();
            if (DownloadCompletedCallback != null)
            {
                DownloadCompletedCallback(uri.OriginalString, saveToFilePath, fileName, e);
            }

        }

        private void progressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            if (ProgressChangedCallBack != null)
            {
                ProgressChangedCallBack.Invoke(fileName, e);
            }
        }
    }
}
