To Setup:
1. Copy Database.mdf to "D:\"
2. download path can be configured via file_downloader\console_tester\App.config (by default it is D:\downloads)
3. Run console_tester project (challenge1.exe) to download the list of URLs
4. Install Web Server for Chrome, and set it to your download folder (via the 'Choose folder' button)
	-Workaround for chrome to access local files. Make sure it is running
5. Run web_module project to run the Web-based GUI
	In the web-based UI, you can:
	-View the list of downloaded files,
	-View the content of the individual downloads
	-View the status of the files (Ready for Processing / Accepted / Rejected)
	-Approve or Reject the file via the buttons
	-Non-viewable files (files other than images, documents, or videos) will be downloaded to client

Solution projects:
-download_module: the DLL that contains the code to download the URLS
-console_tester: triggers the download by consuming the download_module and feeding the list of URLS to it
-Web_module: contains the web controllers, views, and APIs to view the downloaded files, and update their statuses 

Note:
This is my beta version. due to limited time, so some things I could not do within the timeframe:
-change datatype of Status from string to nullable bit in Database
-implement a more elegant way to display the files on the download path
-unit testing
-release the .exe to auto setup the solution.


For a demo, feel free to contact me:
carlnathanmier@gmail.com
+63 922 259 4086

Thank you,
Carl Nathan Mier
