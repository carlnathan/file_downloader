﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using web_module;

namespace web_module.Controllers.api
{
    public class DownloadsController : ApiController
    {
        private DatabaseEntities db = new DatabaseEntities();

        // GET: api/Downloads
        public IQueryable<tblDownload> GettblDownloads()
        {
            return db.tblDownloads;
        }

        // GET: api/Downloads/5
        [ResponseType(typeof(tblDownload))]
        public IHttpActionResult GettblDownload(int id)
        {
            tblDownload tblDownload = db.tblDownloads.Find(id);
            if (tblDownload == null)
            {
                return NotFound();
            }

            return Ok(tblDownload);
        }

        // PUT: api/Downloads/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PuttblDownload(int id, tblDownload tblDownload)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tblDownload.Id)
            {
                return BadRequest();
            }

            db.Entry(tblDownload).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!tblDownloadExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Downloads
        [ResponseType(typeof(tblDownload))]
        public IHttpActionResult PosttblDownload(tblDownload tblDownload)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.tblDownloads.Add(tblDownload);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tblDownload.Id }, tblDownload);
        }

        // DELETE: api/Downloads/5
        [ResponseType(typeof(tblDownload))]
        public IHttpActionResult DeletetblDownload(int id)
        {
            tblDownload tblDownload = db.tblDownloads.Find(id);
            if (tblDownload == null)
            {
                return NotFound();
            }

            db.tblDownloads.Remove(tblDownload);
            db.SaveChanges();

            return Ok(tblDownload);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tblDownloadExists(int id)
        {
            return db.tblDownloads.Count(e => e.Id == id) > 0;
        }
    }
}