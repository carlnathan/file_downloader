﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using web_module;

namespace web_module.Controllers
{
    public class DownloadsApiController : ApiController
    {
        private DatabaseEntities db = new DatabaseEntities();

        // GET: api/DownloadsApi
        public IQueryable<tblDownload> GettblDownloads()
        {
            return db.tblDownloads;
        }

        // POST: api/DownloadsApi
        [ResponseType(typeof(tblDownload))]
        public IHttpActionResult PosttblDownload(tblDownload tblDownload)
        {
            var result = db.tblDownloads.SingleOrDefault(x => x.Id == tblDownload.Id);
            if (result != null)
            {
                result.status = tblDownload.status;
                db.SaveChanges();
                return Ok(result);
            }

            return StatusCode(HttpStatusCode.NotFound);

        }
    }
}