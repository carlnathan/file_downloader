﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using web_module;

namespace web_module.Controllers
{
    public class DownloadsController : Controller
    {
        private DatabaseEntities db = new DatabaseEntities();

        // GET: Downloads
        public ActionResult Index()
        {
            return View(db.tblDownloads.ToList());
        }

        // GET: Downloads/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblDownload tblDownload = db.tblDownloads.Find(id);
            if (tblDownload == null)
            {
                return HttpNotFound();
            }
            return View(tblDownload);
        }

        // GET: Downloads/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Downloads/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,url,filePath,fileName,status")] tblDownload tblDownload)
        {
            if (ModelState.IsValid)
            {
                db.tblDownloads.Add(tblDownload);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tblDownload);
        }

        // GET: Downloads/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblDownload tblDownload = db.tblDownloads.Find(id);
            if (tblDownload == null)
            {
                return HttpNotFound();
            }
            return View(tblDownload);
        }

        // POST: Downloads/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,url,filePath,fileName,status")] tblDownload tblDownload)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tblDownload).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tblDownload);
        }

        // GET: Downloads/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblDownload tblDownload = db.tblDownloads.Find(id);
            if (tblDownload == null)
            {
                return HttpNotFound();
            }
            return View(tblDownload);
        }

        // POST: Downloads/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tblDownload tblDownload = db.tblDownloads.Find(id);
            db.tblDownloads.Remove(tblDownload);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
