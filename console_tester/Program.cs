using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using download_module;
using challenge1;
using System.IO;
using System.Configuration;

namespace console_tester
{
    class Program
    {
        static void Main(string[] args)
        {
            string downloadPath = ConfigurationManager.AppSettings["downloadPath"];

            //re-initialize the db
            using (DatabaseEntities db = new DatabaseEntities())
            {
                db.Database.ExecuteSqlCommand("TRUNCATE TABLE [tblDownloads]");
                db.SaveChanges();
            }
            System.IO.DirectoryInfo di = new DirectoryInfo(downloadPath);

            if (di.Exists)
            {
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
                foreach (DirectoryInfo dir in di.GetDirectories())
                {
                    dir.Delete(true);
                }
            }

            //string path = @"C:\Users\Carl Nathan Mier\Documents\testDL\newFolder";

            string[] sources = new string[] 
            {
                "https://static.standard.co.uk/s3fs-public/thumbnails/image/2016/08/17/12/meme-exhibition.jpg",
                "http://unec.edu.az/application/uploads/2014/12/pdf-sample.pdf",
                "ftp://ftp.funet.fi/pub/standards/RFC/rfc959.txt",
                "http://ipv4.download.thinkbroadband.com/5MB.zip",
                "https://download.microsoft.com/download/5/E/9/5E9B18CC-8FD5-467E-B5BF-BADE39C51F73/SQLServer2017-SSEI-Expr.exe",
                "http://downloads.systoolsgroup.com/mdf-viewer.exe",
                "https://www.visualsvn.com/files/VisualSVN-VS2017-6.2.2.vsix",
                "http://file-examples.com/wp-content/uploads/2018/04/file_example_MOV_1920_2_2MB.mov",
                "https://archive.org/download/ReclaimHtml5/ReclaimHtml5.mp3"
            };

            Downloader downloader = new Downloader();
            downloader.ProgressChangedCallBack += ProgresChanged;
            downloader.DownloadCompleteCallBack += DownloadCompleted;
            downloader.Log += log;
            downloader.Download(sources, downloadPath);
            
            Console.Read();
            

        }

        private static void log(string message)
        {
            Console.WriteLine(message);
        }

        private static void DownloadCompleted(string url, string filePath, string fileName, AsyncCompletedEventArgs e)
        {

            //store to DB
            tblDownload download = new tblDownload()
            {
                url = url,
                filePath = filePath,
                fileName = fileName,
                status = "Ready for Processing"
            };

            using (DatabaseEntities db = new DatabaseEntities())
            {
                db.tblDownloads.Add(download);
                db.SaveChanges();
            }


            //Log to console
            Console.WriteLine("{0} successfully downloaded from {1} to local path {2}", fileName, url, filePath);

        }

        static void ProgresChanged(string fileName, System.Net.DownloadProgressChangedEventArgs e)
        {
            Console.WriteLine("{0} Download progress: {1}%,\t{2}bytes/{3}bytes", fileName, e.ProgressPercentage, e.BytesReceived, e.TotalBytesToReceive);
        }

        

    }
}
